import { combineReducers } from 'redux';
import counterReducer from './counterReducer'
import getUsers from './getUsers'
import getUsersSaga from './getUsersSaga'

const rootReducer = combineReducers({
    counterReducer,
    getUsers,
	getUsersSaga
})

export default rootReducer