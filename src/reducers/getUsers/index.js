import { FETCH_TODOS_REQUEST, FETCH_TODOS_SUCCESS, FETCH_TODOS_FAILURE} from '../../actions/'

const INITIAL_STATE = {
    isFetching: false,
    error: undefined,
    data: undefined
};

let defaultState = {
    isSubmitting: false,
    data: null
}
  
function getUsers(state = INITIAL_STATE, action) {
switch (action.type) {
    case FETCH_TODOS_REQUEST:
    return Object.assign({}, state, {
        isFetching: true
    });
    case FETCH_TODOS_SUCCESS:   
    return Object.assign({}, state, {
        isFetching: false,
        data: action.payload
    });
    case FETCH_TODOS_FAILURE:
    return Object.assign({}, state, {
        isFetching: false,
        error: action.error
    });
    default:
    return state;
}
}

export default getUsers

