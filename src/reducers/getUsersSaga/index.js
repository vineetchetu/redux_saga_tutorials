const INITIAL_STATE = {
    isFetching: false,
    error: undefined,
    data: undefined
};

let defaultState = {
    isSubmitting: false,
    data: null
}
  
function getUsersSaga(state = INITIAL_STATE, action) {
	switch (action.type) {
        case "GET_USERS_START":
            return Object.assign({}, state, {
                isSubmitting: true
            })
        case "GET_USERS_FAILED":
            return Object.assign({}, state, {
                isSubmitting: false
            })
        case "GET_USERS_SUCCEEDED":
            return Object.assign({}, state, {
                isSubmitting: false,
                data: action.payload.response
            })
        default:
            return state
    }
}

export default getUsersSaga

