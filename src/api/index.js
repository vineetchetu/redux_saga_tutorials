// import fetch from 'isomorphic-fetch'

// export default {
//     // API call to get Organization ID of the merchant using Merchant's FB Page ID
//     getUserDetails: function () {
//         return fetch('https://jsonplaceholder.typicode.com/todos/' + 1, {
//             method: 'get',
//             headers: {
//                 'Content-Type': 'application/json'
//             }
//         }).then(response => {
//             const httpStatus = response.status
//             return response.json()
//         })
//     }
// }

//  let endpoint = `${environment.API_URL}/current_user/`

//     let headers = new Headers()
//     headers.append('Authorization', `Bearer ${token.accessToken}`)
//     headers.append('Content-Type', 'application/json')

//     let response = await fetch(endpoint, {
//         method: 'GET',
//         headers: headers
//     })

//     await checkServerResponseForErrors(response)
//     const data = await response.json()
//     checkSerializedResponseForErrors(data)
//     return User.fromAPIObject(data, getCurrentTeam())


export function getUserDetails(){
    return fetch('https://jsonplaceholder.typicode.com/todos/' + 1, {
            method: 'get',
            headers: {
                'Content-Type': 'application/json'
            }
        }).then(response => {
            const httpStatus = response.status
            return response.json()
    })
}