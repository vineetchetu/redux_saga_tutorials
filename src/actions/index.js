import getUserDetails from '../api'
export const ADD_COUNTER = 'ADD_COUNTER'
export const REMOVE_COUNTER = 'REMOVE_COUNTER'
export const FETCH_TODOS_REQUEST = 'FETCH_TODOS_REQUEST'
export const FETCH_TODOS_FAILURE = 'FETCH_TODOS_FAILURE'
export const FETCH_TODOS_SUCCESS = 'FETCH_TODOS_SUCCESS'

export const addCounter = () => ({
  type: ADD_COUNTER,
  payload: 1
});

export const removeCounter = () => ({
  type: REMOVE_COUNTER,
  payload: 1
});

//Using sagas
export const getUsers = () => ({	
	type: 'GET_USERS',
	payload: {
		id: 2
	}
});

//using thunk
export function getUserData(id) {  
  return function(dispatch) {
    dispatch({
      type: FETCH_TODOS_REQUEST
    });
    return fetch('https://jsonplaceholder.typicode.com/todos/'+id, {
            method: 'get',
            headers: {
                'Content-Type': 'application/json'
            }
        }).
        then(response => response.json().then(body => ({ response, body })))
        .then(({ response, body }) => {
          if (!response.ok) {
            dispatch({
              type: FETCH_TODOS_FAILURE,
              error: body.error
            });
          } else {
            dispatch({
              type: FETCH_TODOS_SUCCESS,
              payload: body
            });
        }
      })    
  }
}