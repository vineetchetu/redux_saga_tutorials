import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import { Provider } from 'react-redux';
import { createStore, applyMiddleware } from 'redux';
import reducer from './reducers';
import thunk from 'redux-thunk';
import createSagaMiddleware from 'redux-saga'
import registerServiceWorker from './registerServiceWorker';
import rootSaga from './sagas'

// create the saga middleware
const sagaMiddleware = createSagaMiddleware()

//run the saga
// sagaMiddleware.run(rootSaga)

const store = createStore(
    reducer,
    applyMiddleware(thunk),
	//sagaMiddleware
	applyMiddleware(sagaMiddleware)
);

// run the saga
sagaMiddleware.run(rootSaga)

ReactDOM.render(
    <Provider store={store}>
        <App />
    </Provider>, 
    document.getElementById('root')
);
registerServiceWorker();
