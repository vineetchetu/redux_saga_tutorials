const success = {
	"userId": 2, 
	"id": 2,
	"title": "Chetu",
    "status": "success",
    "message": 0
}

const failed = {
	"userId": 2, 
	"id": 2,
	"title": "Software",
    "status": "error",
    "message": "no_match_found"
}

const responses = [
    success
]

export default function mockGetUsers(id) {
    if (responses.length > 0) {
        return responses[0]
    } else {
        return failed
    }
}
