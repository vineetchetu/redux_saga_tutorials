import React, {Component} from 'react'
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { addCounter, removeCounter, getUserData, getUsers } from '../actions';

class Home extends Component{
    constructor(props){
        super(props)
    }
    
    plusCount = () => {
        this.setState({vineet:'saini'})
        // const { dispatch, count } = this.props
        // dispatch({
        //     type: 'ADD_COUNTER',
        //     payload: 1
        // })
        this.props.dispatch(addCounter())
    }

    minusCount = () => {
        this.props.dispatch(removeCounter())
    }

    userData = () => {        
        this.props.dispatch(getUserData(1))		
    }
	
	userDataSagas = () => { 
		this.props.dispatch(getUsers())
		// const { dispatch,  userDetailsSaga } = this.props
        // dispatch({
            // type: 'GET_USERS',
            // payload: {
                // id: 1
            // }
        // })
    }

    render(){
        let {userDetails, userDetailsSaga} = this.props
        console.log("User data is",userDetails)
		console.log("User userDetailsSaga is", userDetailsSaga)
        return(
            <div>
                <h1>
                    {this.props.addCount}
                    {this.props.removeCount}
                </h1>
				<h3>Using Thunk</h3>
                {userDetails.data !== undefined && 
                    <div>
                        <p>Id- {userDetails.data.id}</p>
                        <p>Title- {userDetails.data.title}</p>                    
                    </div>
                }
				<h3>Using Sagas</h3>
				{userDetailsSaga.data !== undefined && 
                    <div>
                        <p>Id- {userDetailsSaga.data.id}</p>
                        <p>Title- {userDetailsSaga.data.title}</p>                    
                    </div>
                }
                <br/>
                <input type="button" onClick={this.plusCount} value="Add Count" />&ensp;
                <input type="button" onClick={this.minusCount} value="Remove Count" /><br /><br />
                <input type="button" onClick={this.userData} value="Get User Using Thunk" />
				<input type="button" onClick={this.userDataSagas} value="Get User Using Saga" />
            </div>
        );
    }
}

// export default Home

// function mapDispatchToProps(dispatch) {
//     return { actions: bindActionCreators(addCounter, dispatch) }
// }

function mapDispatchToProps(dispatch) {
    let actions = bindActionCreators({ 
        addCounter, 
        removeCounter,
        getUserData,		
    });
    return { ...actions, dispatch };    
}
// export default connect(mapDispatchToProps)(Home);

function mapStateToProps(state){
    return {
        addCount: state.counterReducer,
        userDetails:state.getUsers,
		userDetailsSaga:state.getUsersSaga
    };
}
//   export default connect(mapStateToProps)(Home);

export default connect(mapStateToProps, mapDispatchToProps )(Home)
