//import "babel-polyfill";
import getUsersSaga from './getUsersSaga'
import watchSaga from './watch.js'

export default function* rootSaga() {
    yield [
        getUsersSaga(),        		
		watchSaga()
    ]
}
