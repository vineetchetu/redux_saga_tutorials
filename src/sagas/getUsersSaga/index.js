//import "babel-polyfill";
import {takeLatest, takeEvery, delay} from 'redux-saga'
import {call, put} from 'redux-saga/effects'
//import {push} from 'react-router-redux'
import liveApi from '../../services/api/index.js'
import testApi from '../../services/api/mock.js'

const api = (process.env.test) ? testApi : liveApi
//const api = testApi
export function* getUsersSaga(action) {
    try {
        const response = yield call(api.getUsers, action.payload.id);
        yield put({
            type: 'GET_USERS_SUCCEEDED',
            payload: {
                response: response
            }
        });
    } catch (e) {
        yield put({
            type: 'GET_USERS_FAILED',
            payload: {
                error: e.message
            }
        });
        // if (true) {
            // var errorMsg = 'Error in getUsers API in sagas'
            // console.log(errorMsg)
            // yield put(push({
                    // pathname: '/error-display',
                    // query: {error: errorMsg}
                // })
            // )
        // }
    }
}

export default function* watchGetUers() {
    yield takeLatest('GET_USERS', getUsersSaga)
}
