import mockGetUsers from '../../mock_responses/getUsers.js'

export default {
    getUsers(id) {
        return mockGetUsers(id)
    },    
}