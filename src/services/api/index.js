//import fetch from 'isomorphic-fetch'
//import { LOYALTY_API_URL, MERCHANT_LOYALTY_API_URL, CAMPAIGN_LOYALTY_API_URL, DASHBOARD_LOYALTY_API_URL } from '../../constants'

export default {
    // API call to get Organization ID of the merchant using Merchant's FB Page ID
    getUsers: function (id) {
        return fetch('https://jsonplaceholder.typicode.com/todos/' + id, {
            method: 'get',
            headers: {
                'Content-Type': 'application/json'
            }
        }).then(response => {
            const httpStatus = response.status
            return response.json()
        })
    },   
}
