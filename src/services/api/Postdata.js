//import constants from constant file
import fetch from 'isomorphic-fetch'
import { DASHBOARD_LOYALTY_API_URL } from '../../constants'
export function PostData(type, pageData){
    let BaseURL = DASHBOARD_LOYALTY_API_URL
    return new Promise((resolve, reject) =>{
        fetch(BaseURL+type,{
            method:'POST',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': sessionStorage.pageToken, 
            },
            body:JSON.stringify(pageData)
        })
        .then((response) => response.json())
        .then((responseJson)=> {
            resolve(responseJson);
        })
        .catch((error) => {
            reject(error);
        });
    });
}
